from django.contrib.auth.models import Permission
from django.db import models


class UserPermissions(models.Model):
    codenames = [
        'manager_buildings',
        'manager_transductors'
    ]

    class Meta:
        permissions = (
            ("manager_buildings", "Gerenciar Edifícios"),
            ("manager_transductors", "Gerenciar Transdutores"),
        )

    @classmethod
    def add_all_user_permissions(cls, user):
        codenames = UserPermissions.codenames

        for codename in codenames:
            permission = Permission.objects.get(codename=codename)
            user.user_permissions.add(permission)

    @classmethod
    def remove_all_user_permissions(cls, user):
        codenames = UserPermissions.codenames

        for codename in codenames:
            permission = Permission.objects.get(codename=codename)
            user.user_permissions.remove(permission)

    @classmethod
    def add_user_permissions(cls, user, codenames):
        for codename in codenames:
            permission = Permission.objects.get(codename=codename)
            user.user_permissions.add(permission)

    @classmethod
    def remove_user_permissions(cls, user, codenames):
        for codename in codenames:
            permission = Permission.objects.get(codename=codename)
            user.user_permissions.remove(permission)
