from django.conf.urls import url
from . import views

app_name = 'buildings'
urlpatterns = [
    url(
        r'^novo&campus=(?P<campus_string>[\w\-]+)/$',
        views.new_building,
        name="new_building"
    ),
    url(
        r'^(?P<building_id>[0-9]+)/$',
        views.index,
        name="index"
    ),
    url(
        r'^(?P<building_id>[0-9]+)/info/$',
        views.building_info,
        name="building_info"
    ),
    url(
        r'^(?P<building_id>[0-9]+)/transdutores_defeituosos/$',
        views.building_broken_transductors,
        name="building_broken_transductors"
    ),
    url(
        r'^(?P<building_id>[0-9]+)/edit\&campus=(?P<campus_string>[\w\-]+)/$',
        views.edit_building,
        name="edit_building"
    ),
    url(
        r'^(?P<building_id>[0-9]+)/enable/$',
        views.enable_building,
        name="enable_building"
    ),
    url(
        r'^(?P<building_id>[0-9]+)/disable/$',
        views.disable_building,
        name="disable_building"
    ),
    url(
        r'^(?P<building_id>[0-9]+)/transdutores_desativados/$',
        views.inactive_transductors,
        name="inactive_transductors"
    ),
]
