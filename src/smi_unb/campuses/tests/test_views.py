from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.client import Client

from smi_unb.buildings.models import Building
from smi_unb.transductor.models import EnergyTransductor, TransductorModel
from smi_unb.users.models import UserPermissions
from smi_unb.campuses.models import AdministrativeRegion, Campus


class BuildingsViewsTests(TestCase):
    def setUp(self):
        self.client = Client()

        self.user = User(
            username='test',
            email="test@test.com",
            is_superuser=False,
        )
        self.user.set_password('password')
        self.user.save()

        self.building = self.create_building()

        self.t_model = TransductorModel.objects.create(
            name="TR 4020",
            transport_protocol="UDP",
            serial_protocol="Modbus RTU",
            register_addresses=[[68, 0], [70, 1]],
        )

        self.transductor = EnergyTransductor.objects.create(
            building=self.building,
            model=self.t_model,
            ip_address='1.1.1.1'
        )

    def create_building(self):
        adm_region = AdministrativeRegion.objects.create(
            name="Test Administrative Region"
        )

        campus = Campus.objects.create(
            administrative_region=adm_region,
            name="Test Campus",
            address="Test Address",
            phone="Test Phone",
            url_param="test_campus"
        )

        building = Building.objects.create(
            campus=campus,
            name="Test Building",
            server_ip_address="1.1.1.1"
        )

        return building

    def test_buildings_pages_status_code_with_login(self):
        self.client.login(username='test@test.com', password='password')

        url_index = reverse(
            'campuses:index'
        )
        response_1 = self.client.get(url_index)
        self.assertEqual(200, response_1.status_code)

        url_campus_index = reverse(
            'campuses:campus_index',
            kwargs={'campus_string': self.building.campus.url_param}
        )
        response_2 = self.client.get(url_campus_index)
        self.assertEqual(200, response_2.status_code)

        url_campus_info = reverse(
            'campuses:campus_info',
            kwargs={'campus_string': self.building.campus.url_param}
        )
        response_3 = self.client.get(url_campus_info)
        self.assertEqual(200, response_3.status_code)

    def test_buildings_pages_status_code_without_login(self):
        url_index = reverse(
            'campuses:index'
        )
        response_1 = self.client.get(url_index)
        self.assertEqual(302, response_1.status_code)

        url_campus_index = reverse(
            'campuses:campus_index',
            kwargs={'campus_string': self.building.campus.url_param}
        )
        response_2 = self.client.get(url_campus_index)
        self.assertEqual(302, response_2.status_code)

        url_campus_info = reverse(
            'campuses:campus_info',
            kwargs={'campus_string': self.building.campus.url_param}
        )
        response_3 = self.client.get(url_campus_info)
        self.assertEqual(302, response_3.status_code)

    def test_inactive_buildings(self):
        UserPermissions.add_user_permissions(self.user, ['manager_buildings'])

        self.client.login(username='test@test.com', password='password')

        url = reverse(
            'campuses:inactive_buildings',
            kwargs={'campus_string': self.building.campus.url_param}
        )

        inactive_building = Building.objects.create(
            campus=self.building.campus,
            name="Test Building",
            server_ip_address="1.1.1.2",
            active=False
        )

        response = self.client.get(url)

        self.assertIn(
            inactive_building.name,
            response.content.decode('utf-8')
        )

    def test_inactive_buildings_user_without_permission(self):
        self.client.login(username='test@test.com', password='password')

        url = reverse(
            'campuses:inactive_buildings',
            kwargs={'campus_string': self.building.campus.url_param}
        )

        Building.objects.create(
            campus=self.building.campus,
            name="Test Building",
            server_ip_address="1.1.1.2",
            active=False
        )

        response = self.client.get(url)

        self.assertRedirects(
            response,
            reverse('dashboard')
        )
