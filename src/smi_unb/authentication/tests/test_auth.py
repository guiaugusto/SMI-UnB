from django.contrib.auth.models import User
from django.test import TestCase

from smi_unb.authentication.auth import EmailBackend


class TestUsersAuth(TestCase):
    def setUp(self):
        self.superuser = User(
            username='superuser',
            email="admin@admin.com",
            is_superuser=True,
        )
        self.superuser.set_password('12345')
        self.superuser.save()

    def test_email_backend_authenticate(self):
        email_backend = EmailBackend()

        user = email_backend.authenticate(
            username=self.superuser.email,
            password='12345'
        )

        self.assertEqual(self.superuser, user)

    def test_email_backend_not_authenticate_without_finding_user(self):
        email_backend = EmailBackend()

        email = 'not_existent@email.com'
        password = '12345'

        self.assertRaises(
            User.DoesNotExist,
            email_backend.authenticate(email, password)
        )

    def test_email_backend_not_authenticate_user_not_active(self):
        email_backend = EmailBackend()

        self.superuser.is_active = False
        self.superuser.save()

        user = email_backend.authenticate(
            username=self.superuser.email,
            password='12345'
        )

        self.assertEqual(None, user)

    def test_email_backend_get_user(self):
        email_backend = EmailBackend()

        user = email_backend.get_user(self.superuser.id)

        self.assertEqual(self.superuser, user)

    def test_email_backend_not_get_user(self):
        email_backend = EmailBackend()

        non_existent_id = 999

        self.assertRaises(
            User.DoesNotExist,
            email_backend.get_user(non_existent_id)
        )
